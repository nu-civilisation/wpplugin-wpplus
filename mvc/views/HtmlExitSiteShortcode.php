<?php
/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

class WpplusHtmlExitSiteShortcode {

    public function __construct() {
        add_shortcode('exit_site', array($this, 'doHtmlGet'));
        // ...Rendering the shortcode having the name "enter_site".
        add_action('template_redirect', array($this, 'doHtmlPost'), 1);
        // ...Preprocessing the short code to be able to process the POST request and then redirect to GET again.
        
        // TODO check: if this is working -- seems not so!
        add_action('wp_enqueue_scripts', array($this, 'ensureJQuery'));
        // ...Ensure that JQuery is present.
        add_action('wp_enqueue_scripts', array($this, 'javascriptLogoutAndRedirect'));
        // ...Provide the logout & redirect JavaScript.
        add_filter('wp_nav_menu_items', array($this, 'logoutMenuItem'), 10, 2);
        // ...Add the directly logging out menu item to the pool of menu items.
    }

    public function doHtmlGet($atts = [], $content = '') {
        // Render the shortcode while processing the HTML GET:
        $redirectUrl = get_the_permalink();
        $redirectAfterLogout = (isset($atts['redirect'])) ? $atts['redirect'] : '';
        if(is_user_logged_in()) {
            if(is_page()) {
                // The post is a page -- so render the shortcode as clickable button:
                $content .= '<style>'
                . '.button-primary {'
                . '  padding: 15px 30px;'
                . '  text-align: center;'
                . '  font-weight: bold;'
                . '  font-size: 110%;'
                . '}'
                . '</style>'
                . '<form method="post" action="">'
                . '<input type="submit" name="logout" value="' . __('Logout', 'wpplus') . '." class="button-primary"/>'
                . '<input type="hidden" id="wooplus_exit_site" name="wooplus_exit_site" value=""/>'
                // ...This hidden field is important for the process POST method to process the data.
                . '<input type="hidden" id="redirect_url" name="redirect_url" value="' . $redirectUrl . '"/>'
                . '<input type="hidden" id="redirect_after_logout" name="redirect_after_logout" value="' . $redirectAfterLogout . '"/>'
                . '</form>'
                ;
            }
        }

        return $content;
        // ...Always return the content.
    }

    public function doHtmlPost() {
        // Preprocess the shortcode while processing the HTML POST:
        if(!isset($_POST['wooplus_exit_site'])) return;
        // ...When no redirect-url is set, then there is nothing to do.
        if(!is_page()) return;
        // ...This short code is only supported on WP pages.
        
        global $post;
        $atts = NULL;
        if (!empty($post->post_content)) {
            if(strpos($post->post_content, '[exit_site') !== false) {
                // Preprocess the page:
                wp_logout();

                $redirectUrl = (empty($_POST['redirect_after_logout'])) ? home_url() : $_POST['redirect_after_logout'];
                wp_safe_redirect($redirectUrl);
                exit();
            }
        }
    }

    public function logoutMenuItem($items, $args) {
        if (is_user_logged_in()) {
            // Add a "Log Out" link to the menu
            $items .= '<li class="menu-item"><a href="#" onclick="javascript:logoutAndRedirect();">Log Out</a></li>';
        }

        return $items;
    }

    public function ensureJQuery() {
        wp_enqueue_script('jquery');
    }

    public function javascriptLogoutAndRedirect() {
        $javascript = '
        <script type="text/javascript">
            function logoutAndRedirect() {
                // Use AJAX to log out the user:
                jQuery.post("' . esc_url(admin_url('admin-ajax.php')) . '", { action: "logout" }, function() {
                    // After successful logout, redirect to the home URL:
                    window.location.href = "' . esc_url(home_url()) . '";
                });
            }
        </script>
        ';
        wp_add_inline_script('jquery', $javascript);
    }
}
?>