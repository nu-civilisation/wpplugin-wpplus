<?php
/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

class WpKeyCodes {

    public static function exists() {
        global $wpdb;
        
        require_once(ABSPATH.'wp-admin/includes/upgrade.php');
        
        $charsetCollate = $wpdb->get_charset_collate();
        // ...Be sure to obtain the configured charset!
        $tableName = $wpdb->base_prefix.'wpplus_keycodes';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $sqlString
        = "CREATE TABLE ". $tableName . " ("
        . "key_code VARCHAR(36) NOT NULL, "
        // ...This is an UUIDv4 having the format "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".
        . "user_id INT(20) NOT NULL, "
        // ...This denotes the WP user_id of the owner of this entity.
        . "until VARCHAR(20) NOT NULL, "
        // ...This is a date in UTC string format without any time zones comprising of 20 characters: "YYYY-MM-DDThh:mm:ssZ".
        . "PRIMARY KEY  (key_code), "
        // ...Be sure to have TWO(!) spaces after "PRIMARY KEY"!
        . "KEY index_until (until)"
        . ") $charsetCollate;"
        ;
        dbDelta($sqlString);
        // ...Modify the WP-database based on the SQL statement.
    }

    public static function get($keyCode) {
        global $wpdb;

        $tableName = $wpdb->base_prefix.'wpplus_keycodes';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $sqlString
        = "SELECT key_code, user_id, until"
        . " FROM " . $tableName
        . " WHERE key_code = '" . $keyCode . "'"
        ;

        return $wpdb->get_row($sqlString);
    }

    public static function add($keyCode, $userId, $until) {
        global $wpdb;

        $tableName = $wpdb->base_prefix.'wpplus_keycodes';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $insertData = array
        ( 'key_code' => $keyCode
        , 'user_id'  => $userId
        , 'until'    => $until
        );

        $insertFormat = array('%s', '%d', '%s');
        $wpdb->insert($tableName, $insertData, $insertFormat);
        // ...Inserts the data into the entity table.(
    }

    public static function remove($keyCode) {
        global $wpdb;

        $tableName = $wpdb->base_prefix.'wpplus_keycodes';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $deleteData = array('key_code' => $keyCode);
        $wpdb->delete($tableName, $deleteData);
        // ...Delete the data from the entity table.
    }

    public static function removeExpired() {
        global $wpdb;

        $now = gmdate('Y-m-d\TH:i:s\Z');
        // ...Formats the current timestamp in the format "YYYY-MM-DDThh:mm:ssZ"; these are 20 characters.
        
        $tableName = $wpdb->base_prefix.'wpplus_keycodes';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $sqlString
        = "DELETE FROM " . $tableName
        . " WHERE until < %s"
        ;

        $wpdb->query($wpdb->prepare
            ( $sqlString
            , $now
            )
        );
        // ...Delete the data in the entity table.
    }
}
?>