<?php
/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

require_once(plugin_dir_path(__FILE__) . '../models/WpSites.php');
require_once(plugin_dir_path(__FILE__) . '../models/WpKeyCodes.php');

class WpplusSiteHandling {

    public static function syncSites() {
        WpSites::sync();
    }

    public static function removeExpiredKeyCodes() {
        WpKeyCodes::removeExpired();
    }

    public function __construct() {
        add_action('after_setup_theme', array($this, 'hideAdminBar'));
        //add_action('login_form_register', array($this, 'disableDefaultRegistration'));
        //add_action('wp_login_failed', array($this, 'disableDefaultLogin'));
        //add_action('login_form_lostpassword', array($this, 'disableDefaultPasswordreset'));
        //add_action('template_redirect', array($this, 'redirectPages'));
        //add_filter('check_password', array($this, 'checkPassword'), 10, 3);
        //add_filter('password_hashers', array($this, 'passwordRules'));
    }

    public function disableDefaultRegistration() {
        if(!empty(get_site_option('wpplus_std_login_deactivate'))) {
            remove_action('login_form_register', 'wp_register');
        }
    }

    public function disableDefaultLogin() {
        if(!empty(get_site_option('wpplus_std_register_deactivate'))) {
            wp_die();
        }
    }

    public function disableDefaultPasswordreset() {
        if(!empty(get_site_option('wpplus_std_setpwd_deactivate'))) {
            wp_die();
        }
    }

    public function hideAdminBar() {
        if (!current_user_can('administrator') && !is_admin()) {
            show_admin_bar(false);
        }
    }

    public function redirectPages() {
        //get_edit_user_link(get_current_user_id())
        if(!empty(get_site_option('wpplus_std_profile2woo'))) {
            // The switch for redirecting the standard WP profile to the WooCommerce account page is on:
            if(is_user_logged_in()) {
                $currentUser = wp_get_current_user();
                if(in_array('customer', $currentUser->roles)) {

                    // The current user has the 'customer' role in WooComerce:
                    $currentUrl = home_url($_SERVER['REQUEST_URI']);
                    $profileUrl = get_edit_user_link($currentUser-ID);
                    if($currentUrl == $profileUrl) {
                        // The current URL points to the standard profile URL:
                        $woocommerceActive
                        =  (file_exists(plugin_dir_path(__FILE__) . '../../../woocommerce/woocommerce.php'))
                        // ...Since we are not in the admin context, we can not use the function 'is_plugin_active' and have to test for the file existence instead.
                        && (function_exists('is_plugin_active'))
                        // ...In the multi-site context, when the plugin is activated for this (blog-)site, then the function 'is_plugin_active' is existent.
                        ;
                        if($woocommerceActive) {
                            // Now redirect to the WooCommerce account page:
                            require_once(plugin_dir_path(__FILE__) . '../../../woocommerce/woocommerce.php');
                            $redirectUrl = wc_customer_edit_account_url();
                            // ...The WooCommerce account profile.
                            $this->performRedirect($redirectUrl);
                            // ...Implicitly dies / exits.
                        }
                    }
                }
            }
        }
    }

    public function checkPassword($check, $password, $userId) {
        error_log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> password-check !!!');
        $passwordStrength = wp_check_password_strength('', $password, $userId);
        if($passwordStrength['error']) {
            return new WP_Error('invalid_password', 'The provided password "' . $password . '" does not meet the requirements.');
        }

        return $check;
    }

    public function passwordRules($rules) {
        error_log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> password-rules !!!');
        if(!empty(get_site_option('wpplus_pwd_needed_length'))) {
            $rules['length']['min'] = get_site_option('wpplus_pwd_needed_length');
        }
        if(!empty(get_site_option('wpplus_pwd_needs_number'))) {
            $rules['require_numbers'] = true;
        }
        if(!empty(get_site_option('wpplus_pwd_needs_uppercase'))) {
            $rules['require_uppercase'] = true;
        }
        if(!empty(get_site_option('wpplus_pwd_needs_lowercase'))) {
            $rules['require_lowercase'] = true;
        }
        if(!empty(get_site_option('wpplus_pwd_needs_symbol'))) {
            $rules['require_special_chars'] = true;
        }

        return $rules;
    }

    private function performRedirect($target) {
        switch($target) {
        case '404':
            $this->redirect404();
            die();
            break;
        case 'home':
            wp_redirect(get_home_url());
            die;
            break;
        default:
            global $wp;
            wp_redirect($target);
            // ...The URL-query will be stripped, since the login-redirect works itself with a query.
            die;
            break;
        }
    }

    private function redirect404() {
        global $wp_query;
        
        $wp_query->set_404();
        status_header(404);
        get_template_part(404); 
    }
}
?>