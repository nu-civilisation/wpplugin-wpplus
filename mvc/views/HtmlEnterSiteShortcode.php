<?php
/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

require_once(plugin_dir_path(__FILE__) . '../models/WpSites.php');
require_once(plugin_dir_path(__FILE__) . '../models/WpKeyCodes.php');

class WpplusHtmlEnterSiteShortcode {
    
    public function __construct() {
        add_shortcode('enter_site', array($this, 'doHtmlGet'));
        // ...Rendering the shortcode having the name "enter_site".
        add_action('template_redirect', array($this, 'doHtmlPost'), 1);
        // ...Preprocessing the short code to be able to process the POST request and then redirect to GET again.
    }

    public function doHtmlGet($atts = [], $content = '') {
        // Render the shortcode while processing the HTML GET:
        $redirectAfterLogin = (isset($atts['redirect'])) ? $atts['redirect'] : '';

        $content .= '<style>'
        . 'table {'
        . '  border: none;'            
        . '  width:100%;'
        . '} '
        . 'th, td {'
        . '  border: none;'            
        . '  padding: 5px;'
        . '} '
        . '.no-wrap {'
        . '  white-space: nowrap;'
        . '}'
        . 'input {'
        . '  width: 100%;'
        . '  padding: 5px 5px;'
        . '  box-sizing: border-box;'
        . '}'
        . '.button-cell {'
        . '  text-align: right;'
        . '  vertical-align: middle;'
        . '}'
        . '.button-primary {'
        . '  padding: 15px 30px;'
        . '  text-align: right;'
        . '  font-weight: bold;'
        . '  font-size: 110%;'
        . '}'
        . '.button-secondary {'
        . '  padding: 5px 30px;'
        . '  text-align: right;'
        . '  font-style: italic;'
        . '  font-size: 100%;'
        . '}'
        . '</style>'
        ;

        if(!is_user_logged_in()) {
            $viewMode = (isset($_GET['view_mode'])) ? $_GET['view_mode'] : 'login';

            if($viewMode == 'login') {
                $content .= $this->renderLogin($content, $redirectAfterLogin);
            }
            else if($viewMode == 'register') {
                $content .= $this->renderRegister($content);
            }
            else if($viewMode == 'reset_password') {
                $content .= $this->renderResetPassword($content);
            }
        }

        return $content;
        // ...Always return the content.
    }

    public function doHtmlPost() {
        // Preprocess the shortcode while processing the HTML POST:
        if(!isset($_POST['wooplus_enter_site'])) return;
        // ...When no 'wooplus_enter_site' is set, then there is nothing to do.
        if(!is_page()) return;
        // ...This short code is only supported on WP pages.
        
        global $post;
        $atts = NULL;
        if (!empty($post->post_content)) {
            if(strpos($post->post_content, '[enter_site') !== false) {
                // Preprocess the page:
                $viewMode = isset($_POST['view_mode']) ? $_POST['view_mode'] : 'login';

                $atts = NULL;
                if($viewMode === 'login') {
                    $atts = $this->processLogin();
                } else if($viewMode === 'register') {
                    $atts = $this->processRegister();
                } else if($viewMode === 'reset_password') {
                    $atts = $this->processResetPassword();
                }

                $redirectUrl = ((isset($atts['logged_in'])) && ($atts['logged_in'] == 'yes')) ? $atts['redirect'] : add_query_arg($atts, $_POST['redirect_url']);
                // ...If not logged in, then build the redirect URL with query parameters.
                wp_safe_redirect($redirectUrl);
                // ...Redirect to the same page with the query parameters.
                exit();
            }
        }
    }

    private function renderLogin($content, $redirectAfterLogin) {
        $redirectUrl = get_the_permalink();
        $userName    = isset($_GET['username']) ? urldecode($_GET['username']) : '';
        $password    = isset($_GET['password']) ? urldecode($_GET['password']) : '';

        if(isset($_GET['registered'])) $content .= '<ul><li>' . __('Successfully registered. Now provide your e-mail-address and set your password here.', 'wpplus') . '</li></ul>';
        if(isset($_GET['logged_in'])) $content .= '<ul><li>' . __('Invalid username / e-mail & password combination.', 'wpplus') . '</li></ul>';
        if(isset($_GET['no_email'])) $content .= '<ul><li>' . __('No valid e-mail-address for the password reset provided.', 'wpplus') . '</li></ul>';
        
        $content .= '<form method="post" action="">'
        . '<table>'
        . '<tr><td class="no-wrap">'
        . __('Username / E-Mail', 'wpplus') . ':'
        . '</td><td colspan="2">'
        . '<input type="text" name="username" value="' . $userName . '"/>'
        . '</td></tr>'
        . '<tr><td class="no-wrap">'
        . __('Password', 'wpplus') . ':'
        . '</td><td colspan="2">'
        . '<input type="password" name="password" value="' . $password . '"/>'
        . '</td></tr>'
        . '<tr><td></td><td style="width: 50%;"></td>'
        . '<td style="padding: 5px;" class="button-cell">'
        . '<input type="submit" name="login" value="' . __('Login', 'wpplus') . '." class="button-primary"/>'
        . '</td></tr>'
        . '<tr><tr><td></td><td style="width: 50%;"></td>'
        . '<td style="padding: 5px;" class="button-cell">'
        . '<input type="submit" name="register" value="' . __('Register', 'wpplus') . '..." class="button-secondary"/>'
        . '</td></tr><tr><td></td><td style="width: 50%;"></td>'
        . '<td style="padding: 5px;" class="button-cell">'
        . '<input type="submit" name="reset_password" value="' . __('(Re)set password', 'wpplus') . '..." class="button-secondary"/>'
        . '</td></tr>'
        . '<input type="hidden" id="wooplus_enter_site" name="wooplus_enter_site" value=""/>'
        // ...This hidden field is important for the process POST method to process the data.
        . '<input type="hidden" id="redirect_url" name="redirect_url" value="' . $redirectUrl . '"/>'
        . '<input type="hidden" id="redirect_after_login" name="redirect_after_login" value="' . $redirectAfterLogin . '"/>'
        . '<input type="hidden" id="view_mode" name="view_mode" value="login"/>'
        // ...The current view-mode has to be specified for the following HTML POST processing.
        . '</table>'
        . '</form>'
        ;
        
        return $content;
        // ...Always return the content.
    }

    private function processLogin() {
        $atts = array();
        $userName = isset($_POST['username']) ? urldecode($_POST['username']) : '';
        $password = isset($_POST['password']) ? urldecode($_POST['password']) : '';
        $redirect = isset($_POST['redirect_after_login']) ? urldecode($_POST['redirect_after_login']) : '';
        if(isset($_POST['login'])) {
            if($this->loginUser($userName, $password)) {
                $atts['logged_in'] = 'yes';
                $redirectUrl = '';
                if(empty($redirect) || ($redirect == 'home')) {
                    // The user is already successfully logged in, so redirect to the site home:
                    $redirectUrl = home_url();
                }
                else if($redirect == 'profile') {
                    // The user is already successfully logged in, so redirect to his profile:
                    $woocommerceActive
                    =  (file_exists(plugin_dir_path(__FILE__) . '../../../woocommerce/woocommerce.php'))
                    // ...Since we are not in the admin context, we can not use the function 'is_plugin_active' and have to test for the file existence instead.
                    && (function_exists('is_plugin_active'))
                    // ...In the multi-site context, when the plugin is activated for this (blog-)site, then the function 'is_plugin_active' is existent.
                    ;
                    if($woocommerceActive) {
                        require_once(plugin_dir_path(__FILE__) . '../../../woocommerce/woocommerce.php');
                        $redirectUrl = wc_customer_edit_account_url();
                        // ...The WooCommerce account profile.
                    }
                    else {
                        $redirectUrl = get_edit_user_link(get_current_user_id());
                        // ...The standard Wordpress user profile.
                    }
                }
                else {
                    $redirectUrl = $redirect;
                }
                $atts['redirect'] = $redirectUrl;
            }
            else {
                $atts['logged_in'] = 'no';
            }
            $atts['view_mode'] = 'login';
        }
        else if(isset($_POST['register'])) {
            $atts['view_mode'] = 'register';
        }
        else if(isset($_POST['reset_password'])) {
            if((isset($_POST['username']) && is_email($userName))) {
                $user = get_user_by('email', $userName);
                if(!empty($user)) {
                    $userId = $user->ID;
                    $validitySeconds = get_site_option('wpplus_keycode_validity') * 60;
                    $keyCode = wp_generate_uuid4();
                    $now     = gmdate('Y-m-d\TH:i:s\Z');
                    $until   = gmdate('Y-m-d\TH:i:s\Z', strtotime($now) + $validitySeconds);
                    // ...900 seconds are 15 minutes.
                    // ...Formats the current timestamp in the format "YYYY-MM-DDThh:mm:ssZ"; these are 20 characters.
                    WpKeyCodes::add($keyCode, $userId, $until);
                    $this->mailKeyCode($userId, $keyCode);
                }
                $atts['email']     = $userName;
                $atts['view_mode'] = 'reset_password';
            }
            else {
                $atts['no_email'] = '';
                $atts['view_mode'] = 'login';
            }
        }

        return $atts;
        // ...Always return the atts.
    }

    private function renderRegister($content) {
        $redirectUrl = get_the_permalink();
        $firstName   = isset($_GET['firstname']) ? urldecode($_GET['firstname']) : '';
        $lastName    = isset($_GET['lastname']) ? urldecode($_GET['lastname']) : '';
        $eMail       = isset($_GET['email']) ? urldecode($_GET['email']) : '';

        $content .= '<form method="post" action=""><ul>';
        if(isset($_GET['invalid_firstname'])) $content .= '<li>'.__('Invalid first name: the first name must not be emtpy.', 'wpplus').'</li>';
        if(isset($_GET['invalid_lastname'])) $content .= '<li>'.__('Invalid last name: the last name must not be emtpy.', 'wpplus').'</li>';
        if(isset($_GET['invalid_email'])) $content .= '<li>'.__('Invalid e-mail: please provide a valid e-mail.', 'wpplus').'</li>';
        if(isset($_GET['existing_email'])) $content .= '<li>'.__('Existing e-mail: this e-mail is already existing; please provide another one.', 'wpplus').'</li>';
        $content .= '</ul>'
        . '<table>'
        . '<tr><td class="no-wrap">'
        . __('First Name', 'wpplus') . ':'
        . '</td><td colspan="2">'
        . '<input type="text" name="firstname" value="' . $firstName . '"/>'
        . '</td></tr>'
        . '<tr><td class="no-wrap">'
        . __('Last Name', 'wpplus') . ':'
        . '</td><td colspan="2">'
        . '<input type="text" name="lastname" value="' . $lastName . '"/>'
        . '</td></tr>'
        . '<tr><td class="no-wrap">'
        . __('E-Mail', 'wpplus') . ':'
        . '</td><td colspan="2">'
        . '<input type="text" name="email" value="' . $eMail . '"/>'
        . '</td></tr>'
        . '<tr><td></td><td></td style="width: 50%;">'
        . '<td style="padding: 5px;" class="button-cell">'
        . '<input type="submit" name="register" value="' . __('Next', 'wpplus') . '..." class="button-primary"/>'
        . '</td></tr><tr><td></td><td style="width: 50%;"></td>'
        . '<td style="padding: 5px;" class="button-cell">'
        . '<input type="submit" name="back_login" value="' . __('Back to login', 'wpplus') . '..." class="button-secondary"/>'
        . '</td></tr>'
        . '<input type="hidden" id="wooplus_enter_site" name="wooplus_enter_site" value=""/>'
        // ...This hidden field is important for the process POST method to process the data.
        . '<input type="hidden" id="redirect_url" name="redirect_url" value="' . $redirectUrl . '"/>'
        . '<input type="hidden" id="view_mode" name="view_mode" value="register"/>'
        // ...The current view-mode has to be specified for the following HTML POST processing.
        . '</table>'
        .'</form>'
        ;
        
        return $content;
        // ...Always return the content.
    }

    private function processRegister() {
        // We have the registration data, now verify it:
        $atts      = array();
        $firstName = $_POST['firstname'];
        $lastName  = $_POST['lastname'];
        $email     = $_POST['email'];

        if(isset($_POST['back_login'])) {
            $atts['view_mode'] = 'login';
        }
        else {
            $isValid = true;
            // ...Assume it.
            if(empty($firstName)) {
                $atts['invalid_firstname'] = '';
                $isValid = false;
            }
            else {
                $atts['firstname'] = urlencode($firstName);
            }
            if(empty($lastName)) {
                $atts['invalid_lastname'] = '';
                $isValid = false;
            }
            else {
                $atts['lastname'] = urlencode($lastName);
            }
            if(!is_email($email)) {
                $atts['invalid_email'] = '';
                $isValid = false;
            }
            if(email_exists($email)) {
                $atts['existing_email'] = '';
                $isValid = false;
            }

            if($isValid) {
                $this->registerUser($firstName, $lastName, $email);
                // ...Create the user.
                // ...may throw: \Exception -- when wp_insert_user() fails.
                $atts['registered'] = '';
                $atts['username'] = $email;
                $atts['view_mode'] = 'login';
            }
            else {
                $atts['view_mode'] = 'register';
            }
        }


        return $atts;
        // ...Always return the atts.
    }

    private function renderResetPassword($content) {
        $redirectUrl = get_the_permalink();
        $mailCode  = isset($_GET['mailcode']) ? urldecode($_GET['mailcode']) : '';
        $password1 = isset($_GET['password1']) ? urldecode($_GET['password1']) : '';
        $password2 = isset($_GET['password2']) ? urldecode($_GET['password2']) : '';
        $email     = isset($_GET['email']) ? urldecode($_GET['email']) : '';

        $content .= '<ul>';
        if(isset($_GET['no_code'])) $content .= '<li>' . __('The key-code must not be empty.', 'wpplus') . '</li>';
        if(isset($_GET['password_mismatch'])) $content .= '<li>' . __('The two passwords mismatch.', 'wpplus') . '</li>';
        if(isset($_GET['password_too_weak'])) $content .= '<li>' . __('The provided passwords are too weak.', 'wpplus') . '</li>';
        if(isset($_GET['password_too_short'])) $content .= '<li>' . __('The provided passwords are too short.', 'wpplus') . '</li>';
        if(isset($_GET['password_no_number'])) $content .= '<li>' . __('The provided passwords do not contain a number.', 'wpplus') . '</li>';
        if(isset($_GET['password_no_uppercase'])) $content .= '<li>' . __('The provided passwords do not contain an uppercase letter.', 'wpplus') . '</li>';
        if(isset($_GET['password_no_lowercase'])) $content .= '<li>' . __('The provided passwords do not contain a lowercase letter.', 'wpplus') . '</li>';
        if(isset($_GET['password_no_symbol'])) $content .= '<li>' . __('The provide passwords do not contain a symbol.', 'wpplus') . '</li>';
        $content .= '</ul>';

        $content .= '<form method="post" action="">'
        . '<table>'
        . '<tr><td class="no-wrap">'
        . __('Key-code sent to your e-mail-address', 'wpplus') . ':'
        . '</td><td colspan="2">'
        . '<input type="text" name="mailcode" value="' . $mailCode . '"/>'
        . '</td></tr>'
        . '<tr><td class="no-wrap">'
        . __('Set your password', 'wpplus') . ':'
        . '</td><td colspan="2">'
        . '<input type="password" name="password1" value="' . $password1 . '"/>'
        . '</td></tr>'
        . '<tr><td class="no-wrap">'
        . __('Repeat your password', 'wpplus') . ':'
        . '</td><td colspan="2">'
        . '<input type="password" name="password2" value="' . $password2 . '"/>'
        . '</td></tr>'
        . '<tr><td></td><td style="width: 50%;"></td><td style="padding: 5px;" class="button-cell">'
        . '<input type="submit" name="reset_password" value="' . __('Next', 'wpplus') . '..." class="button-primary"/>'
        . '<input type="hidden" id="wooplus_enter_site" name="wooplus_enter_site" value=""/>'
        // ...This hidden field is important for the process POST method to process the data.
        . '<input type="hidden" id="redirect_url" name="redirect_url" value="' . $redirectUrl . '"/>'
        . '<input type="hidden" id="email" name="email" value="' . $email . '"/>'
        . '<input type="hidden" id="view_mode" name="view_mode" value="reset_password"/>'
        // ...The current view-mode has to be specified for the following HTML POST processing.
        . '</td></tr>'
        . '</table>'
        . '</form>'
        ;
        
        return $content;
        // ...Always return the content.
    }

    private function processResetPassword() {
        $atts = array();
        $mailCode  = isset($_POST['mailcode']) ? $_POST['mailcode'] : '';
        $password1 = isset($_POST['password1']) ? $_POST['password1'] : '';
        $password2 = isset($_POST['password2']) ? $_POST['password2'] : '';
        $email     = isset($_POST['email']) ? $_POST['email'] : '';

        if(!empty($mailCode)) {
            if($password1 == $password2) {
                // TODO rework this completely: the function "wp_check_password_strength" does not exist !!!
//                $passwordCheck    = wp_check_password_strength($password1);
//                $passwordStrength = $passwordCheck['strength'];
//                $passwordErrors   = $passwordCheck['errors'];
                $passwordAcceptable = true;
//                if($passwordStrength < get_site_option('wpplus_pwd_needed_strength')) {
//                    $atts['password_too_weak'] = '';
//                    $passwordAcceptable = false;
//                }
//                if(!empty($passwordErrors)) {
//                    if(!empty($passwordErrors['password_too_short']) && get_site_option('wpplus_pwd_needs_length')) {
//                        $atts['password_too_short'] = '';
//                        $passwordAcceptable = false;
//                    }
//                    if(!empty($passwordErrors['password_no_number']) && get_site_option('wpplus_pwd_needs_number')) {
//                        $atts['password_no_number'] = '';
//                        $passwordAcceptable = false;
//                    }
//                    if(!empty($passwordErrors['password_no_uppercase']) && get_site_option('wpplus_pwd_needs_uppercase')) {
//                        $atts['password_no_uppercase'] = '';
//                        $passwordAcceptable = false;
//                    }
//                    if(!empty($passwordErrors['password_no_lowercase']) && get_site_option('wpplus_pwd_needs_lowercase')) {
//                        $atts['password_no_lowercase'] = '';
//                        $passwordAcceptable = false;
//                    }
//                    if(!empty($passwordErrors['password_no_symbol']) && get_site_option('wpplus_pwd_needs_symbol')) {
//                        $atts['password_no_symbol'] = '';
//                        $passwordAcceptable = false;
//                    }
//                }
                if($passwordAcceptable) {
                    $keyCode = WpKeyCodes::get($mailCode);
                    if(!empty($keyCode->key_code)) {
                        $userId = $keyCode->user_id;
                        wp_set_password($password1, $userId);
                        WpKeyCodes::remove($mailCode);
                    }
                    $atts['username'] = $email;
                    $atts['view_mode'] = 'login';
                }
                else {
                    $atts['view_mode'] = 'reset_password';
                }
            }
            else {
                $atts['password_mismatch'] = '';
                $atts['view_mode'] = 'reset_password';
            }
        }
        else {
            $atts['no_code'] = '';
            $atts['view_mode'] = 'reset_password';
        }
 
        return $atts;
        // ...Always return the atts.
    }

    private function registerUser($firstName, $lastName, $email) {
        $userLogin = $this->uniqueWpUserName($firstName, $lastName);
        // ...Ensure that the new WP user-login is unique and canonised.
        $userPassword = wp_generate_password(64, true, true);
        // ...Generate a highly cryptic password.
        // ...The user gets it via mail or needs to obtain a new one via "forgot password".
        $userData = array();
        $userData['user_login'] = $userLogin;
        $userData['user_pass'] = $userPassword;
        $userData['user_email'] = $email;
        // ...It was already checked that the e-mail is unique.
        $userData['first_name'] = $firstName;
        $userData['last_name'] = $lastName;
        $userId = wp_insert_user($userData);
        // ...Creates the user; we have checked, that he not exists.
        // ...The configured default role will be assigned.
        if(is_a($userId, 'WP_Error')) {
            throw new \Exeption();
        }
        WpSites::sync();
        // ...Ensure, that the user is in all multi-sites.

        wp_new_user_notification($userId, null, 'user');
        // ...Be sure to trigger the registration notification: to send a welcome mail to the user.
    }

    private function loginUser($wpUserLogin, $wpUserPassword) {
        $credentials = array();
        $credentials['user_login'] = $wpUserLogin;
        $credentials['user_password'] = $wpUserPassword;
        $credentials['remember'] = true;
        $wpuser = wp_signon($credentials, false);
        // ...Authenticates and logs the user in with ‘remember’ capability.
        if(!is_wp_error($wpuser)) {
            $wpuserId = $wpuser->ID;
            wp_set_current_user($wpuserId, $wpUserLogin);
            // ...Changes the current user by ID or name.
            wp_set_auth_cookie($wpuserId, true, false);
            // ...Log in a user by setting authentication cookies.
            do_action('wp_login', $wpUserLogin, $wpuser);
            // ...Fires after the user has successfully logged in.

            return true;
        }
        else {
            return false;
        }
    }

    private function uniqueWpUserName($firstName, $lastName) {
        $loginName
        = strtolower(preg_replace('/[^\w]/', '', iconv("utf-8","ascii//TRANSLIT",$firstName)))
          // ...Ensure that only lowercase alpha characters are used; any umlaut characters are mapped!
        . "."
        . strtolower(preg_replace('/[^\w]/', '', iconv("utf-8","ascii//TRANSLIT",$lastName)))
          // ...Ensure that only lowercase alpha characters are used; any umlaut characters are mapped!
        ;

        $wpuser = get_user_by('login', $loginName);
        if($wpuser) {
            // Such a username already exists, append the least number:
            $index = 0;
            $newLoginName = $loginName;
            do {
                $index++;
                $newLoginName = $loginName.".".$index;
                $wpUser = get_user_by('login', $newLoginName);
            }
            while($wpUser);

            $loginName = $newLoginName;
        }

        return $loginName;
        // ...Now it is ensured that the new, canonised WP user-name is unique.
    }
    
    private function mailKeyCode($userId, $keyCode) {
        $user        = get_userdata($userId);
        $mailAddress = (empty($user->user_email)) ? NULL : $user->user_email;
        if(!empty($mailAddress)) {
            $replacements = array();
            $replacements['key_code'] = $keyCode;
            $replacements['user_name'] = $user->user_login;
            $replacements['first_name'] = $user->first_name;
            $replacements['last_name'] = $user->last_name;
            $replacements['e_mail'] = $mailAddress;

            $templateMailSubject = get_option('wpplus_keycode_mail_subject');
            $templateMailBody    = get_option('wpplus_keycode_mail_body');
            $mailSubject         = $this->replaceText($templateMailSubject, $replacements);
            $mailBody            = $this->replaceText($templateMailBody, $replacements);
        
            wp_mail($mailAddress, $mailSubject, $mailBody);
        }
    }

    private function replaceText($text, $replacements) {
        foreach($replacements as $key => $value) {
            $text = str_replace('{' . $key . '}', $value, $text);
        }

        return $text;
    }
}
?>