<?php
/**
 * @wordpress-plugin
 * Plugin Name:       WpPlus
 * Plugin URI:        https://www.nu-civilisation.org/
 * Description:       Adds really essential functionality to WordPress. Especially for multisites. -- Amasing!
 * Version:           1.0.0
 * Author:            NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation
 * Author URI: 		  https://www.nu-civilisation.org
 * License:           GPL-3.0
 * License URI:       https://www.gnu.org/licenses/gpl-3.0.en.html
 * Text Domain:       wpplus
 */

/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

require_once(plugin_dir_path(__FILE__) . 'mvc/models/WpSites.php');
require_once(plugin_dir_path(__FILE__) . 'mvc/models/WpKeyCodes.php');
require_once(plugin_dir_path(__FILE__) . 'mvc/views/AdminPageSiteManagement.php');
require_once(plugin_dir_path(__FILE__) . 'mvc/views/HtmlEnterSiteShortcode.php');
require_once(plugin_dir_path(__FILE__) . 'mvc/views/HtmlExitSiteShortcode.php');
require_once(plugin_dir_path(__FILE__) . 'mvc/views/HtmlAdminSiteShortcode.php');
require_once(plugin_dir_path(__FILE__) . 'mvc/controllers/WpSiteHandling.php');

class WpPlusPlugin {

    private $wpAdminPageSiteManagement;
    // ...The admin page for site management.
    private $wpHtmlEnterSiteShortcode;
    // ...The enter site shortcode enclosed in the WpplusHtmlEnterSiteShorcode class.
    private $wpHtmlExitSiteShortcode;
    // ...The exit site shortcode enclosed in the WpplusHtmlEnterSiteShorcode class.
    private $wpHtmlAdminSiteShortcode;
    // ...The admin site shortcode enclosed in the WpplusHtmlAdminSiteShorcode class.
    private $wpSiteHandling;
    // ...The WordPress site handling enclosed in the WpplusSiteHanding class.

    public function __construct() {
        $this->wpAdminPageSiteManagement = new WpplusAdminPageSiteManagement();
        // ...Encapsule the admin page for site mangement.
        $this->wpHtmlEnterSiteShortcodes = new WpplusHtmlEnterSiteShortcode();
        // ...Encapsule the enter site shortcodes in an own class.
        $this->wpHtmlExitSiteShortcodes = new WpplusHtmlExitSiteShortcode();
        // ...Encapsule the exit site shortcodes in an own class.
        $this->wpHtmlAdminSiteShortcodes = new WpplusHtmlAdminSiteShortcode();
        // ...Encapsule the admin site shortcodes in an own class.
        $this->wpSiteHandling = new WpplusSiteHandling();
        // ...Encapsule the WordPress site handling in an own class.

        add_filter('plugin_action_links_' . plugin_basename(__FILE__), array($this, 'linkAdminSiteManagement'), 10, 1);
        // ...Add the WpPlus site management page link to the plugins list.
        add_filter('cron_schedules', array($this, 'cronInterval5min'));
        // ...Add the 5 minute interval to the possible intervals.
    }

    public function activate() {
        WpSites::exists();
        // ...Registeres the hooks for handling the WordPress mulit-sites.
        WpKeyCodes::exists();
        // ...Creates or modifies -- if necessary -- the GENERAL 'wpplus_keycodes' entity table.

        add_action('wpplus_cron_hourly', array($this, 'runCronHourly'));
        // ...Run the cron function for site syncing.
        // ...It is important to have the add_action function call exactly on this place!
        add_action('wpplus_cron_5mins', array($this, 'runCron5mins'));
        // ...Run the cron function for site syncing.
        // ...It is important to have the add_action function call exactly on this place!
        if(!wp_next_scheduled('wpplus_cron_hourly')) {
            wp_schedule_event(time(), 'hourly', 'wpplus_cron_hourly');
            // ...Enregister the site syncing cron activity to WP-Cron.
        }
        if(!wp_next_scheduled('wpplus_cron_5mins')) {
            wp_schedule_event(time(), 'every_5_minutes', 'wpplus_cron_5mins');
            // ...Enregister the purging key-codes cron activity to WP-Cron.
        }

        $this->ensureTranslations();
    }

    public function deactivate() {
        $timestamp = wp_next_scheduled('wpplus_cron_hourly');
        if($timestamp) {
            wp_unschedule_event($timestamp, 'wpplus_cron_hourly');
            // ...Unregister the site syncing cron activity from WP-Cron.
        }
        $timestamp = wp_next_scheduled('wpplus_cron_5mins');
        if($timestamp) {
            wp_unschedule_event($timestamp, 'wpplus_cron_5mins');
            // ...Unregister the site syncing cron activity from WP-Cron.
        }
    }

    public function linkAdminSiteManagement($links) {
        $toolsLink = '<a href="tools.php?page=wpplusSiteManagement">' . __('Site Management', 'wpplus') . '</a>';
        array_push($links, $toolsLink);
        
        return $links;
    }

    public function cronInterval5min() {
        $schedules['every_5_minutes'] = array
        ( 'interval' => 300
        // ...300 seconds are 5 minutes
        , 'display'  => __('Every 5 Minutes', 'wpplus')
        );

        return $schedules;
    }

    public function runCronHourly() {
        WpSiteHandling::syncSites();
        // ...Syncs the sites.
    }

    public function runCron5mins() {
        WpSiteHandling::syncSites();
        // ...Syncs the sites.
    }

    private function ensureTranslations() {
        $i18nFolderPath        = plugin_dir_path(__FILE__) . 'i18n/';
        $destinationFolderPath = plugin_dir_path(__FILE__) . '../../languages/plugins/';
        $moFiles = glob($i18nFolderPath . 'wpplus-*.mo');
        foreach ($moFiles as $moFile) {
            $moBasename = basename($moFile);
            copy($moFile, $destinationFolderPath . $moBasename);
        }
    }
}

$wpplusPlugin = new WpPlusPlugin();
register_activation_hook(__FILE__, array($wpplusPlugin, 'activate'));
register_activation_hook(__FILE__, 'WpSites::sync');
register_deactivation_hook(__FILE__, array($wpplusPlugin, 'deactivate'));
?>
