<?php
/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

class WpplusHtmlAdminSiteShortcode {

    public function __construct() {
        add_shortcode('admin_site', array($this, 'doHtmlGet'));
        // ...Rendering the shortcode having the name "admin_site".
        add_action('template_redirect', array($this, 'doHtmlPost'), 1);
        // ...Preprocessing the short code to be able to process the POST request and then redirect to GET again.
    }

    public function doHtmlGet($atts = [], $content = '') {
        // Render the shortcode while processing the HTML GET:
        if(is_user_logged_in()) {
            if (current_user_can('manage_options')) {
                $redirectUrl = admin_url('index.php');
                // ...The admin pages dashboard.

                $content .= '<style>'
                . '.button-primary {'
                . '  padding: 15px 30px;'
                . '  text-align: right;'
                . '  font-weight: bold;'
                . '  font-size: 110%;'
                . '}'
                . '</style>'
                . '<form method="post" action="">'
                . '<input type="submit" name="admin" value="' . __('Admin Site', 'wpplus') . '..." class="button-primary"/>'
                . '<input type="hidden" id="wooplus_admin_site" name="redirect_url" value=""/>'
                // ...This hidden field is important for the process POST method to process the data.
                        . '<input type="hidden" id="redirect_url" name="redirect_url" value="' . $redirectUrl . '"/>'
                . '</form>'
                ;
            }
        }

        return $content;
        // ...Always return the content.
    }

    public function doHtmlPost() {
        // Preprocess the shortcode while processing the HTML POST:
        if(!isset($_POST['wooplus_admin_site'])) return;
        // ...When no 'wooplus_admin_site' is set, then there is nothing to do.
        if(!is_page()) return;
        // ...This short code is only supported on WP pages.
        
        global $post;
        $atts = NULL;
        if (!empty($post->post_content)) {
            if(strpos($post->post_content, '[admin_site') !== false) {
                // Preprocess the page:
                $redirectUrl = $_POST['redirect_url'];
                wp_safe_redirect($redirectUrl);
                exit();
            }
        }
    }
}
?>