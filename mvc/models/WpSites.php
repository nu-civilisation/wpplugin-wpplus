<?php
/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

class WpSites {

    public static function exists() {
        add_action('set_user_role', 'WpSites::add', 10, 3);
        add_action('set_user_role', 'WpSites::addUser', 10, 3);
    }

    public static function sync() {
        global $wpdb;

        if(is_multisite()) {
            $tableName = $wpdb->base_prefix . 'blogs';
            // ...Be sure to obtain the database prefix to be consistent in naming!
            // ...The entity table is a WordPress blog site GENERAL table!
            $sqlString
            = "SELECT blog_id"
            . " FROM " . $tableName
            ;
            $siteIds = $wpdb->get_col($sqlString);
            $currentSiteId = get_current_blog_id();
            $users = get_users(array('blog_id' => $currentSiteId, 'fields' => 'all_with_meta'));

            remove_action('set_user_role', 'WpSites::addUser', 10, 3);
            foreach($users as $user) {
                foreach($siteIds as $siteId) {
                    if(is_array($user->roles) && ($user->roles)) {
                        $roles = $user->roles;
                        foreach($roles as $role) {
                            add_user_to_blog($siteId, $user->ID, $role);
                        }
                    }
                    else {
                        add_user_to_blog($siteId, $user->ID, get_blog_option($siteId, 'default_role', 'subscriber' ));
                    }
                }
            }
            add_action('set_user_role', 'WpSites::addUser', 10, 3);
        }
    }

    public static function add($newSiteId) {
        global $wpdb;

        $tableName = $wpdb->base_prefix . 'blogs';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $sqlString
        = "SELECT blog_id"
        . " FROM " . $tableName
        . " LIMIT 2"
        ;
        $siteIds = $wpdb->get_col($sqlString);

        remove_action('set_user_role', 'WpSites::addUser', 10, 3);
        foreach($siteIds as $siteId) {
            if($siteId != $newSiteId) {
                $users = get_users(array('blog_id' => $siteId, 'fields' => 'all_with_meta'));
                foreach($users as $user) {
                    if(is_array($user->roles) && ($user->roles)) {
                        $roles = $user->roles;
                        foreach($roles as $role) {
                            add_user_to_blog($siteId, $user->ID, $role);
                        }
                    }
                }
            }
        }
        add_action('set_user_role', 'WpSites::addUser', 10, 3);
    }

    public static function addUser($userId, $newRole, $previousRoles) {
        global $wpdb;

        if(is_multisite()) {
            $tableName = $wpdb->base_prefix . 'blogs';
            // ...Be sure to obtain the database prefix to be consistent in naming!
            // ...The entity table is a WordPress blog site GENERAL table!
            $sqlString
            = "SELECT blog_id"
            . " FROM " . $tableName
            ;
            $siteIds = $wpdb->get_col($sqlString);

            remove_action('set_user_role', 'WpSites::addUser', 10, 3);
            foreach($siteIds as $siteId) {
                add_user_to_blog($siteId, $userId, $newRole);
            }
            add_action('set_user_role', 'WpSites::addUser', 10, 3);
        }
    }
}
?>