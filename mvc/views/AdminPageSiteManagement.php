<?php
/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

require_once(plugin_dir_path(__FILE__) . '../models/WpSites.php');

class WpplusAdminPageSiteManagement {
    
    public function __construct() {
        add_action('admin_menu', array($this, 'addSubmenuPage'));
        add_action('admin_post_wpplusSaveChangesPwds', array($this, 'saveChangesPasswordPolicy'));
        add_action('admin_post_wpplusSaveChangesKeycodes', array($this, 'saveChangesKeyCodeMgmt'));
        add_action('admin_post_wpplusSaveChangesPages', array($this, 'saveChangesStdPages'));
        add_action('admin_post_wpplusSyncSites', array($this, 'syncWpSites'));
    }

    public function addSubmenuPage() {
        add_submenu_page
        ( 'options-general.php'
        // ...The parent menu; here: the "Tools" menu.
        , __('Site Management', 'wpplus')
        // ...The text that will be displayed as the submenu page's title in the admin menu.
        , __('Site Management', 'wpplus')
        // ...The text that will be displayed in the browser's title bar when the submenu page is open.
        , 'manage_options'
        // ...The capability required for a user to access this submenu page.
        , 'wpplusSiteManagement'
        // ...The unique slug.
        , array($this, 'displaySiteManagement')
        // ...The callback function.
        );
        // ...Adds a submenu page to the WordPress admin menu.
    }

    public function displaySiteManagement() {
        echo '<style>'
        . 'table {'
        . '  width:100%;'
        . '} '
        . 'th, td {'
        . '  padding: 5px;'
        . '} '
        . 'input {'
        . '  width: 100%;'
        . '  padding: 5px 5px;'
        . '  box-sizing: border-box;'
        . '}'
        . 'textarea {'
            . '  width: 100%;'
            . '  padding: 5px 5px;'
            . '  box-sizing: border-box;'
            . '}'
            . '.no-wrap {'
        . '  white-space: nowrap;'
        . '}'
        . '.text-cell {'
        . '  text-align: left;'
        . '  vertical-align: middle;'
        . '}'
        . '.button-cell {'
        . '  text-align: right;'
        . '  vertical-align: middle;'
        . '}'
        . '.button-primary {'
        . '  padding: 15px 30px;'
        . '  font-size: 110%;'
        . '}'
        . '</style>'
        ;
        echo '<div class="wrap">';

        $activeTab      = isset($_GET['tab']) ? $_GET['tab'] : 'tabPassw';
        $tabPasswActive = ($activeTab == 'tabPassw') ? ' nav-tab-active' : '';
        $tabKCodeActive = ($activeTab == 'tabKCode') ? ' nav-tab-active' : '';
        $tabPagesActive = ($activeTab == 'tabPages') ? ' nav-tab-active' : '';
        $tabUsersActive = ($activeTab == 'tabUsers') ? ' nav-tab-active' : '';

        echo '<div class="wrap">';
        echo '<h2>' . __('Site Management', 'wooplus') . '</h2>';
        echo '<nav class="nav-tab-wrapper">';
        echo '<a href="?page=wpplusSiteManagement&tab=tabPassw" class="nav-tab' . $tabPasswActive . '">' . __('Password Policy', 'wpplus') . '</a>';
        echo '<a href="?page=wpplusSiteManagement&tab=tabKCode" class="nav-tab' . $tabKCodeActive . '">' . __('Key-Code Management', 'wpplus') . '</a>';
        echo '<a href="?page=wpplusSiteManagement&tab=tabPages" class="nav-tab' . $tabPagesActive . '">' . __('Standard Pages', 'wpplus') . '</a>';
        echo '<a href="?page=wpplusSiteManagement&tab=tabUsers" class="nav-tab' . $tabUsersActive . '">' . __('User Management', 'wpplus') . '</a>';
        echo '</nav>';

        switch ($activeTab) {
        case 'tabPassw':
            $this->displayTabPasswordPolicy();
            break;
        case 'tabKCode':
            $this->displayTabKeyCodeManagement();
            break;
        case 'tabPages':
            $this->displayTabStandardPages();
            break;
        case 'tabUsers':
            $this->displayTabUserManagement();
            break;
        default:
            // Never happens.
        }

        echo '</div>';
    }

    public function saveChangesPasswordPolicy() {
        if (!current_user_can('manage_options')) {
            wp_die(__('You are not allowed to access this page.', 'wpplus'));
        }
        check_admin_referer('wpplusSiteMgmtNonce');

        $neededPasswordLength = (empty($_POST['pwd_needed_length'])) ? $this->defaultPasswordLength() : $_POST['pwd_needed_length'];
        if($neededPasswordLength < 3) {
            $neededPasswordLength = 3;
        }
        if($neededPasswordLength > 64) {
            $neededPasswordLength = 64;
        }
        update_site_option('wpplus_pwd_needed_length', $neededPasswordLength);
        if(isset($_POST['pwd_needs_number'])) {
            update_site_option('wpplus_pwd_needs_number', 'yes');
        }
        else {
            delete_site_option('wpplus_pwd_needs_number');
        }
        if(isset($_POST['pwd_needs_uppercase'])) {
            update_site_option('wpplus_pwd_needs_uppercase', 'yes');
        }
        else {
            delete_site_option('wpplus_pwd_needs_uppercase');
        }
        if(isset($_POST['pwd_needs_lowercase'])) {
            update_site_option('wpplus_pwd_needs_lowercase', 'yes');
        }
        else {
            delete_site_option('wpplus_pwd_needs_lowercase');
        }
        if(isset($_POST['pwd_needs_symbol'])) {
            update_site_option('wpplus_pwd_needs_symbol', 'yes');
        }
        else {
            delete_site_option('wpplus_pwd_needs_symbol');
        }

        $neededPasswordStrength = (empty($_POST['pwd_needed_strength'])) ? $this->defaultPasswordStrength() : $_POST['pwd_needed_strength'];
        if($neededPasswordStrength < 0) {
            $neededPasswordStrength = 0;
        }
        if($neededPasswordStrength > 5) {
            $neededPasswordStrength = 5;
        }
        update_site_option('wpplus_pwd_needed_strength', $neededPasswordStrength);

        wp_redirect(admin_url('options-general.php?page=wpplusSiteManagement&tab=tabPassw'));
        exit;
    }

    public function saveChangesKeyCodeMgmt() {
        if (!current_user_can('manage_options')) {
            wp_die(__('You are not allowed to access this page.', 'wpplus'));
        }
        check_admin_referer('wpplusSiteMgmtNonce');

        $keycodeValitity = (empty($_POST['keycode_validity'])) ? $this->defaultKeycodeValidity() : $_POST['keycode_validity'];
        if($keycodeValitity < 5) {
            $keycodeValitity = 5;
        }
        if($keycodeValitity > 60) {
            $keycodeValitity = 60;
        }
        update_site_option('wpplus_keycode_validity', $keycodeValitity);
        $keycodeMailSubject = (empty($_POST['keycode_mail_subject'])) ? $this->defaultKeycodeMailSubject() : $_POST['keycode_mail_subject'];
        update_option('wpplus_keycode_mail_subject', $keycodeMailSubject);
        // ...Deliberately store the key-code mail subject template in the (blog-)site specific options!
        $keycodeMailBody = (empty($_POST['keycode_mail_body'])) ? $this->defaultKeycodeMailBody() : $_POST['keycode_mail_body'];
        update_option('wpplus_keycode_mail_body', $keycodeMailBody);
        // ...Deliberately store the key-code mail body template in the (blog-)site specific options!

        wp_redirect(admin_url('options-general.php?page=wpplusSiteManagement&tab=tabKCode'));
        exit;
    }

    public function saveChangesStdPages() {
        if (!current_user_can('manage_options')) {
            wp_die(__('You are not allowed to access this page.', 'wpplus'));
        }
        check_admin_referer('wpplusSiteMgmtNonce');

        if(isset($_POST['std_login_deactivate'])) {
            update_site_option('wpplus_std_login_deactivate', 'yes');
        }
        else {
            delete_site_option('wpplus_std_login_deactivate');
        }
        if(isset($_POST['std_register_deactivate'])) {
            update_site_option('wpplus_std_register_deactivate', 'yes');
        }
        else {
            delete_site_option('wpplus_std_register_deactivate');
        }
        if(isset($_POST['std_setpwd_deactivate'])) {
            update_site_option('wpplus_std_setpwd_deactivate', 'yes');
        }
        else {
            delete_site_option('wpplus_std_setpwd_deactivate');
        }
        if(isset($_POST['std_backend_hide'])) {
            update_site_option('wpplus_std_backend_hide', 'yes');
        }
        else {
            delete_site_option('wpplus_std_backend_hide');
        }
        if(isset($_POST['std_profile2woo'])) {
            update_site_option('wpplus_std_profile2woo', 'yes');
        }
        else {
            delete_site_option('wpplus_std_profile2woo');
        }

        wp_redirect(admin_url('options-general.php?page=wpplusSiteManagement&tab=tabPages'));
        exit;
    }

    public function syncWpSites() {
        if (!current_user_can('manage_options')) {
            wp_die(__('You are not allowed to access this page.', 'wpplus'));
        }
        check_admin_referer('wpplusSiteMgmtNonce');

        WpSites::sync();

        wp_redirect(admin_url('options-general.php?page=wpplusSiteManagement&tab=tabUsers&synced'));
        exit;
    }

    private function displayTabPasswordPolicy() {
        $neededPwdLength           = (empty(get_site_option('wpplus_pwd_needed_length'))) ? $this->defaultPasswordLength() : get_site_option('wpplus_pwd_needed_length');
        $checkedPwdNeedsNumber     = (empty(get_site_option('wpplus_pwd_needs_number'))) ? '' : ' checked';
        $checkedPwdNeedsUppercase  = (empty(get_site_option('wpplus_pwd_needs_uppercase'))) ? '' : ' checked';
        $checkedPwdNeedsLowercase  = (empty(get_site_option('wpplus_pwd_needs_lowercase'))) ? '' : ' checked';
        $checkedPwdNeedsSymbol     = (empty(get_site_option('wpplus_pwd_needs_symbol'))) ? '' : ' checked';
        $neededPwdStrength         = (empty(get_site_option('wpplus_pwd_needed_strength'))) ? $this->defaultPasswordStrength() : get_site_option('wpplus_pwd_needed_strength');

        echo '<div class="wrap">';
        echo '<form method="post" action="admin-post.php">';
        echo '<table><tr>';
        echo '<td colspan="2"><h2>' . __('Password Requirements', 'wpplus') . '</h2></td>';
        echo '</tr><tr>';
        echo '<td style="text-align: right; vertical-align: text-top;"><input type="number" name="pwd_needed_length" min="3" max="64" value="' . $neededPwdLength . '"/>';
        echo '<td style="width: 90%;">' . __('The passwords need to have at least this length. The values can be between 3 and 64.', 'wpplus') . '</td>';
        echo '</tr><tr>';
        echo '<td style="text-align: right;"><input type="checkbox" name="pwd_needs_number"' . $checkedPwdNeedsNumber . '/>';
        echo '<td style="width: 90%;" class="no-wrap">' . __('The passwords need to have at least one number.', 'wpplus') . '</td>';
        echo '</tr><tr>';
        echo '<td style="text-align: right;"><input type="checkbox" name="pwd_needs_uppercase"' . $checkedPwdNeedsUppercase . '/>';
        echo '<td style="width: 90%;" class="no-wrap">' . __('The passwords need to have at least one uppercase letter.', 'wpplus') . '</td>';
        echo '</tr><tr>';
        echo '<td style="text-align: right;"><input type="checkbox" name="pwd_needs_lowercase"' . $checkedPwdNeedsLowercase . '/>';
        echo '<td style="width: 90%;" class="no-wrap">' . __('The passwords need to have at least one lowercase letter.', 'wpplus') . '</td>';
        echo '</tr><tr>';
        echo '<td style="text-align: right;"><input type="checkbox" name="pwd_needs_symbol"' . $checkedPwdNeedsSymbol . '/>';
        echo '<td style="width: 90%;" class="no-wrap">' . __('The passwords need to have at least one symbol.', 'wpplus') . '</td>';
        echo '</tr><tr>';
        echo '<td colspan="2"><h2>' . __('Password Strength', 'wpplus') . '</h2></td>';
        echo '</tr><tr>';
        echo '<td style="text-align: right; vertical-align: text-top;"><input type="number" name="pwd_needed_strength" min="0" max="5" value="' . $neededPwdStrength . '"/>';
        echo '<td style="width: 90%;">';
        echo __('The passwords need to have at least this strength. The values can be between 0 and 5, The values represent:', 'wpplus');
        echo '<p><ul>';
        echo '<li><b>0 (' . __('Very Weak', 'wpplus') . ')</b>: ' . __('A password with a strength of 0 is considered very weak. It likely fails to meet most or all of the password strength requirements, such as length, including numbers, uppercase and lowercase letters, and special characters.', 'wpplus') . '</li>';
        echo '<li><b>1 (' . __('Weak', 'wpplus') . ')</b>: ' . __('A password with a strength of 1 is still weak but may meet some minimal requirements, such as having a minimum length, but it may lack other necessary elements like numbers or special characters.', 'wpplus') . '</li>';
        echo '<li><b>2 (' . __('Moderate', 'wpplus') . ')</b>: ' . __('A password with a strength of 2 is of moderate strength. It usually meets basic requirements for length, includes a mix of uppercase and lowercase letters, and may include numbers or symbols.', 'wpplus') . '</li>';
        echo '<li><b>3 (' . __('Good', 'wpplus') . ')</b>: ' . __('A password with a strength of 3 is considered good. It typically meets most standard password requirements, including length, uppercase and lowercase letters, numbers, and special characters.', 'wpplus') . '</li>';
        echo '<li><b>4 (' . __('Strong', 'wpplus') . ')</b>: ' . __('A password with a strength of 4 is strong and meets stringent password requirements. It has a sufficient length, a good mix of characters, and may include complex combinations of letters, numbers, and symbols.', 'wpplus') . '</li>';
        echo '<li><b>5 (' . __('Very Strong', 'wpplus') . ')</b>: ' . __('A password with a strength of 5 is the strongest possible. It exceeds standard requirements and includes a highly diverse combination of characters, making it very difficult to guess or crack through brute-force methods.', 'wpplus') . '</li>';
        echo '</ul></td>';
        echo '</tr><tr>';
        echo '<td style="text-align: right;"><input type="submit" value="'. __('Save Changes.', 'wpplus') . '"class="button-primary"/></td>';
        echo '</tr></table>';
        echo '<input type="hidden" name="action" value="wpplusSaveChangesPwds"/>';
        wp_nonce_field('wpplusSiteMgmtNonce');
        echo '</form>';
        echo '</div>';
    }

    private function displayTabKeyCodeManagement() {
        $keycodeValitity    = (empty(get_site_option('wpplus_keycode_validity'))) ? $this->defaultKeycodeValidity() : get_site_option('wpplus_keycode_validity');
        $keycodeMailSubject = (empty(get_option('wpplus_keycode_mail_subject'))) ? $this->defaultKeycodeMailSubject() : get_option('wpplus_keycode_mail_subject');
        $keycodeMailBody    = (empty(get_option('wpplus_keycode_mail_body'))) ? $this->defaultKeycodeMailBody() : get_option('wpplus_keycode_mail_body');

        echo '<div class="wrap">';
        echo '<form method="post" action="admin-post.php">';
        echo '<table><tr>';
        echo '<td colspan="2"><h2>' . __('Key-Code Validity in Minutes', 'wpplus') . '</h2></td>';
        echo '</tr><tr>';
        echo '<td style="text-align: right; vertical-align: text-top;"><input type="number" name="keycode_validity" min="5" max="60" value="' . $keycodeValitity . '"/></td>';
        echo '<td style="width: 90%;">' . __('The number of minutes the key-code for (re)setting the password; between 5 and 60 minutes.', 'wpplus') . '</td>';
        echo '</tr></table>';
        echo '<hr>';
        echo '<table><tr>';
        echo '<td colspan="3"><h2>'
        . __('Key-Code Mail Template', 'wpplus')
        . '</h2>'
        . __('These mail settings below are not muli-site global, but specific to every site.', 'wpplus')
        . '</td>'
        ;
        echo '</tr><tr>';
        echo '<td colspan ="2"><input type="text" id="keycode_mail_subject" name="keycode_mail_subject" value="' . $keycodeMailSubject . '"/></td>';
        echo '<td style="text-align: left;">' . __('The mail subject. You can apply the replacements here.', 'wpplus' . '</td>');
        echo '</tr><tr>';
        echo '<td colspan="2"><textarea id="keycode_mail_body" name="keycode_mail_body" rows="10" cols="50">' . $keycodeMailBody . '</textarea></td>';
        echo '<td style="text-align: left; vertical-align: text-top;">'
        . __
        ( 'The mail body. You can apply the following replacements here:<ul>'
        . '<li><b>{key_code}</b></li>'
        . '<li>{user_name}</li>'
        . '<li>{first_name}</li>'
        . '<li>{last_name}</li>'
        . '<li>{e_mail}</li>'
        . '<ul>'
        , 'wpplus')
        . '</td>'
        ;
        echo '</tr><tr>';
        echo '<td style="text-align: left;"><input type="submit" value="'. __('Save Changes.', 'wpplus') . '"class="button-primary"/></td>';
        echo '<td style="width: 50%;"></td><td></td>';
        echo '</tr></table>';
        echo '<input type="hidden" name="action" value="wpplusSaveChangesKeycodes"/>';
        wp_nonce_field('wpplusSiteMgmtNonce');
        echo '</form>';
        echo '</div>';
    }

    private function displayTabStandardPages() {
        $checkedStdLoginDeactivate    = (empty(get_site_option('wpplus_std_login_deactivate'))) ? '' : ' checked';
        $checkedStdRegisterDeactivate = (empty(get_site_option('wpplus_std_register_deactivate'))) ? '' : ' checked';
        $checkedStdSetPwdDeactivate   = (empty(get_site_option('wpplus_std_setpwd_deactivate'))) ? '' : ' checked';
        $checkedStdBackendBarHide     = (empty(get_site_option('wpplus_std_backend_hide'))) ? '' : ' checked';
        $checkedStdProfile2Woo        = (empty(get_site_option('wpplus_std_profile2woo'))) ? '' : ' checked';

        echo '<div class="wrap">';
        echo '<form method="post" action="admin-post.php">';
        echo '<table><tr>';
        echo '<td colspan="2"><h2>'
        . __('Standard Pages Deactivation', 'wpplus')
        . '</h2>'
        . __('When ticking one of these boxes, you need to have an [enter_site] shortcode and an [admin_site] shortcode on publicly accessible pages.', 'wpplus')
        . '</td>'
        ;
        echo '</tr><tr>';
        echo '<td style="text-align: right;"><input type="checkbox" name="std_login_deactivate"' . $checkedStdLoginDeactivate . '/>';
        echo '<td style="width: 90%;" class="no-wrap">' . __('Deactivate the standard login function.', 'wpplus') . '</td>';
        echo '</tr><tr>';
        echo '<td style="text-align: right;"><input type="checkbox" name="std_register_deactivate"' . $checkedStdRegisterDeactivate . '/>';
        echo '<td style="width: 90%;" class="no-wrap">' . __('Deactivate the standard register function.', 'wpplus') . '</td>';
        echo '</tr><tr>';
        echo '<td style="text-align: right;"><input type="checkbox" name="std_setpwd_deactivate"' . $checkedStdSetPwdDeactivate . '/>';
        echo '<td style="width: 90%;" class="no-wrap">' . __('Deactivate the standard (re)set password function.', 'wpplus') . '</td>';
        echo '</tr></table>';
        echo '<table><tr>';
        echo '<td colspan="2"><h2>' . __('Standard Backend Handling', 'wpplus') . '</h2></td>';
        echo '</tr><tr>';
        echo '<td style="text-align: right;"><input type="checkbox" name="std_backend_hide"' . $checkedStdBackendBarHide . '/>';
        echo '<td style="width: 90%;" class="no-wrap">' . __('Hide the standard backend bar to non admins.', 'wpplus') . '</td>';
        echo '</tr><tr>';
        echo '<td style="text-align: right;"><input type="checkbox" name="std_profile2woo"' . $checkedStdProfile2Woo . '/>';
        echo '<td style="width: 90%;" class="no-wrap">' . __('Redirect from the standard profile page to the WooCommerce account page.', 'wpplus') . '</td>';
        echo '</tr><tr>';
        echo '<td style="text-align: left;"><input type="submit" value="'. __('Save Changes.', 'wpplus') . '"class="button-primary"/></td>';
        echo '<td style="width: 50%;"></td><td></td>';
        echo '</tr></table>';
        echo '<input type="hidden" name="action" value="wpplusSaveChangesPages"/>';
        wp_nonce_field('wpplusSiteMgmtNonce');
        echo '</form>';
        echo '</div>';
    }
    
    private function displayTabUserManagement() {
        echo '<div class="wrap">';
        echo '<form method="post" action="admin-post.php">';
        echo '<table><tr>';
        if(isset($_GET['synced'])) {
            echo '<td><div id="message" class="updated fade">';
            echo '<p><strong>' . __('All users on all multi-sites were synched.', 'wpplus') . '</strong></p>';
            echo '</div></td>';
            echo '</tr><tr>';
        }
        echo '<td><h2>'
        . __('Multi-Site-Wide User Synchronisation', 'wpplus')
        . '</h2>'
        . __('Synchronize all users on all multi-sites now:', 'wpplus')
        . '</td>';
        echo '</tr><tr>';
        echo '<td>';
        echo '<input type="submit" value="' . __('Sync.', 'wpplus') . '" class="button-primary"/>';
        echo '<input type="hidden" name="action" value="wpplusSyncSites"/>';
        echo '</td>';
        wp_nonce_field('wpplusSiteMgmtNonce');
        echo '</form>';
        echo '</div>';
    }

    private function defaultPasswordLength() {
        return 8;
    }

    private function defaultPasswordStrength() {
        return 3;
    }

    private function defaultKeycodeValidity() {
        return 15;
    }

    private function defaultKeycodeMailSubject() {
        return __('Your key-code for the password (re)set', 'wpplus');
    }

    private function defaultKeycodeMailBody() {
        return __
        ( 'Hello {first_name},'
        . '<p><p><p>'
        . 'your key-code for (re)setting your password is:'
        . '<h2>{key_code}</h2>'
        . 'Good luck!'
        , 'wpplus')
        ;
    }
}
?>